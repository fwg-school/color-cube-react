import React, { useState, useEffect } from "react";
import { filter, noop } from "lodash";

import { Props } from "./types";
import { SearchInput } from "./style";
import { colors } from "./__mocks__/colors";

const Search = (props: Props) => {
  const { onSearch = noop } = props;

  // Getter & Setter
  const [searchValue, setSearchValue] = useState(""); // Hooks

  useEffect(() => {
    const filteredColor = filter(colors, (color) =>
      color.name.toLowerCase().includes(searchValue.toLowerCase())
    );
    onSearch(filteredColor);
  }, [searchValue]);

  return (
    <SearchInput
      placeholder="Search by color name"
      value={searchValue}
      onChange={(e) => {
        setSearchValue(e.target.value);
      }}
    />
  );
};

export default Search;
