import { Color } from "../../../types/color";

export const colors: Array<Color> = [
    {
        name: "Red",
        value: "#F00"
    },
    {
        name: "Blue",
        value: [0,0,255]
    },
    {
        name: "Dark Blue",
        value: "#00008b"
    }
]